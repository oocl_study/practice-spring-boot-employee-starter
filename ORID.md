O:
At the beginning of the morning, we had a contextmap group collaboration on the TDD we learned last week, and then started learning new knowledge points: WebServer, including HTTP and Restful. Then we learned the pair program, and finally we learned SpringBoot. I collaborated with Charlie on a project and experienced the benefits of the pair program.
R:
Feel-well
I:
No problem at the moment
D:
I think the pair program can quickly identify errors and complement each other's shortcomings, which is very practical in agile development.