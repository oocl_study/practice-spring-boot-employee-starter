package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.entity.Employee;

import java.util.List;


public interface EmployeeService {

    Employee getEmployeeById(Integer id);

    List<Employee> getEmployees();

    List<Employee> getEmployeeBySex(String sex);

    List<Employee> getEmployeesByCompanyId(Integer id);


    Integer addEmployee(Employee employee);

    void deleteEmployee(Integer id);

    void modifyEmployee(Employee employeeParam);

    List<Employee> pageEmployees(Integer page, Integer size);
}
