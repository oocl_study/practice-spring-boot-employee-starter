package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public interface CompanyService {



    List<Company> getAllCompanies();

    Company getCompanyById(Integer id);

    List<Employee> getEmployeesById(Integer id);

    List<Company> pageCompanies(Integer page, Integer size);

    Integer addCompany(Company company);

    void updateCompany(Company companyParam);

    void deleteCompany(Integer id);
}
