package com.thoughtworks.springbootemployee.service.impl;

import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.service.EmployeeService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private ArrayList<Employee> employees = new ArrayList<>(Arrays.asList(
            new Employee(1, "Charlie", "male", 20, 100000, 1),
            new Employee(2, "Garrick", "male", 20, 100000, 1),
            new Employee(3, "Jaden", "male", 20, 100000, 2))
    );

    @Override
    public Employee getEmployeeById(Integer id) {
        Employee dbEmployee = employees.stream().filter(employee -> employee.getId() == id).findFirst().get();
        return dbEmployee;
    }

    @Override
    public List<Employee> getEmployees() {
        return employees;
    }

    @Override
    public List<Employee> getEmployeeBySex(String sex) {
        return employees.stream().filter(employee -> sex.equals(employee.getSex())).collect(Collectors.toList());
    }

    @Override
    public List<Employee> getEmployeesByCompanyId(Integer id) {
        return employees.stream().filter(employee -> employee.getCompanyId() == id).collect(Collectors.toList());
    }

    public Integer getNextId() {
        return employees.stream()
                .max(Comparator.comparingInt(Employee::getId))
                .get()
                .getId() + 1;
    }

    @Override
    public Integer addEmployee(Employee employee) {
        employee.setId(getNextId());
        employees.add(employee);
        return employee.getId();
    }

    @Override
    public void deleteEmployee(Integer id) {
        employees.remove(employees.stream().filter(employee -> employee.getId() == id).findFirst().get());
    }

    @Override
    public void modifyEmployee(Employee employeeParam) {
        Employee dbEmployee = employees.stream().filter(employee -> employee.getId() == employeeParam.getId()).findFirst().get();
        BeanUtils.copyProperties(employeeParam, dbEmployee);
    }

    @Override
    public List<Employee> pageEmployees(Integer page, Integer size) {
        int fromIndex = (page - 1) * size;
        int toIndex = (fromIndex + size) > employees.size() ? employees.size() : (fromIndex + size);
        return employees.subList(fromIndex, toIndex);
    }
}
