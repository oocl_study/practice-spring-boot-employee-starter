package com.thoughtworks.springbootemployee.service.impl;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.service.CompanyService;
import com.thoughtworks.springbootemployee.service.EmployeeService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

@Service
public class CompanyServiceImpl implements CompanyService {

    public List<Company> companyList = new ArrayList<>(Arrays.asList(
            new Company(1 ,"spring"),
            new Company(2 ,"summer"),
            new Company(3 ,"autumn"),
            new Company(4 ,"winter")
    ));

    @Autowired
    private EmployeeService employeeService;

    @Override
    public List<Company> getAllCompanies() {
        return companyList;
    }

    @Override
    public Company getCompanyById(Integer id) {
        return companyList.stream().filter(company -> company.getId() == id).findFirst().orElse(null);
    }

    @Override
    public List<Employee> getEmployeesById(Integer id) {
        return employeeService.getEmployeesByCompanyId(id);
    }

    @Override
    public List<Company> pageCompanies(Integer page, Integer size) {
        int fromIndex = (page - 1) * size;
        int toIndex = (fromIndex + size) > companyList.size() ? companyList.size() : (fromIndex + size);
        return companyList.subList(fromIndex, toIndex);
    }

    public Integer getNextId() {
        return companyList.stream()
                .max(Comparator.comparingInt(Company::getId))
                .get()
                .getId() + 1;
    }

    @Override
    public Integer addCompany(Company company) {
        company.setId(getNextId());
        companyList.add(company);
        return company.getId();
    }

    @Override
    public void updateCompany(Company companyParam) {
        Company dbCompany = companyList.stream().filter(company -> company.getId() == companyParam.getId()).findFirst().get();
        BeanUtils.copyProperties(companyParam, dbCompany);
    }

    @Override
    public void deleteCompany(Integer id) {
        companyList.remove(companyList.stream().filter(company -> company.getId() == id).findFirst().get());
    }
}
