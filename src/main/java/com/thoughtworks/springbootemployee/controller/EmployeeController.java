package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/employees")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping
    public List<Employee> getEmployees() {
        return employeeService.getEmployees();
    }

    @GetMapping(path = "{id}")
    public Employee getEmployeeById(@PathVariable Integer id) {
        return employeeService.getEmployeeById(id);
    }

    @GetMapping(params = "sex")
    public List<Employee> getEmployeeBySex(@RequestParam String sex) {
        if(sex == null) return null;
        return employeeService.getEmployeeBySex(sex);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Integer addEmployee(@RequestBody Employee employee){
        return employeeService.addEmployee(employee);
    }

    @PutMapping
    public void modifyEmployee(@RequestBody Employee employee){
        employeeService.modifyEmployee(employee);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployee(@PathVariable Integer id){
        employeeService.deleteEmployee(id);
    }

    @GetMapping(params = {"page", "size"})
    public List<Employee> pageEmployees(@RequestParam Integer page, @RequestParam Integer size){
        return employeeService.pageEmployees(page, size);
    }

}
