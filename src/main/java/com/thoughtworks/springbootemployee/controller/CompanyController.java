package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("companies")
public class CompanyController {

    @Autowired
    private CompanyService companyService;


    @GetMapping()
    public List<Company> getCompanies(){
        return companyService.getAllCompanies();
    }

    @GetMapping("/{id}")
    public Company getCompanyById(@PathVariable Integer id){
        return companyService.getCompanyById(id);
    }

    @GetMapping("/{id}/employees")
    public List<Employee> getEmployeesByCompany(@PathVariable Integer id){
        return companyService.getEmployeesById(id);
    }

    @GetMapping(params = {"page", "size"})
    public List<Company> pageCompanies(@RequestParam Integer page, @RequestParam Integer size){
        return companyService.pageCompanies(page, size);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Integer addCompany(@RequestBody Company company){
        return companyService.addCompany(company);
    }


    @PutMapping
    public void updateCompany(@RequestBody Company company){
        companyService.updateCompany(company);
    }

    @DeleteMapping(params = {"id"})
    public void deleteCompany(@RequestParam Integer id){
        companyService.deleteCompany(id);
    }
}
